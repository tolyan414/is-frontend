# is-front

### Что это

Адаптивный виджет курсов валют

### Как работает

Получает данные курсов валют с [бэкенда](https://bitbucket.org/tolyan414/is-backend/src/master/) и показывает на веб странице

### Как запустить

Сначала локально запустить [бэкенд](https://bitbucket.org/tolyan414/is-backend/src/master/)

Затем

     git clone git@bitbucket.org:tolyan414/is-frontend.git
     cd is-frontend
     npm i
     npm run serve

### Как запустить в докере

На машине с установленными docker & docker-compose в пустой папке:

    git clone git@bitbucket.org:tolyan414/is-frontend.git
    git clone git@bitbucket.org:tolyan414/is-backend.git
    cd is-frontend/docker
    ./start.sh

Открываем в браузере http://localhost:8090

### Для сборки

Прописать url для бэкенда в .env.production и затем

     npm run build

### Подробности

При разработке приложение показывает пару виджетов разного размера на странице. При установленной переменной окружения ONE_WIDGET_ONLY на странице будет один виджет на всю ширину. Можно использовать в iframe.

Адаптивность. При изменении ширины страницы количество табов не обновляется. Это лечится обновлением страницы. Вообще, можно добавить обработчик события типа onSizeChanged и в нем изменять количество табов
