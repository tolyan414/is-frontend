### Как запустить в докере

На машине с установленными docker & docker-compose в пустой папке:

    git clone git@bitbucket.org:tolyan414/is-frontend.git
    git clone git@bitbucket.org:tolyan414/is-backend.git
    cd is-frontend/docker
    ./start.sh

Открываем в браузере http://localhost:8090