#!/bin/bash

cd ../../is-backend
npm i
cd ../is-frontend
npm i
npm run build
cd ./docker

docker-compose up # -d
